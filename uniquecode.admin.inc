<?php

/**
 * Generates the uniquecode type editing form.
 */
function uniquecode_type_form($form, &$form_state, $uniquecode_type, $op = 'edit') {

  if ($op == 'clone') {
    $uniquecode_type->label .= ' (cloned)';
    $uniquecode_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $uniquecode_type->label,
    '#description' => t('The human-readable name of this uniquecode type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($uniquecode_type->type) ? $uniquecode_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $uniquecode_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'uniquecode_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this uniquecode type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#title' => t('Description'),
    '#type' => 'textarea',
    '#default_value' => isset($uniquecode_type->description) ? $uniquecode_type->description : '',
    '#description' => t('Description about the uniquecode type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save uniquecode type'),
    '#weight' => 40,
  );

  if (!$uniquecode_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete uniquecode type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('uniquecode_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing uniquecode_type.
 */
function uniquecode_type_form_submit(&$form, &$form_state) {
  $uniquecode_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  uniquecode_type_save($uniquecode_type);

  // Redirect user back to list of uniquecode types.
  $form_state['redirect'] = 'admin/structure/uniquecode-types';
}

function uniquecode_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/uniquecode-types/' . $form_state['uniquecode_type']->type . '/delete';
}

/**
 * Unique Code type delete form.
 */
function uniquecode_type_form_delete_confirm($form, &$form_state, $uniquecode_type) {
  $form_state['uniquecode_type'] = $uniquecode_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['uniquecode_type_id'] = array('#type' => 'value', '#value' => entity_id('uniquecode_type' ,$uniquecode_type));
  return confirm_form($form,
    t('Are you sure you want to delete uniquecode type %title?', array('%title' => entity_label('uniquecode_type', $uniquecode_type))),
    'uniquecode/' . entity_id('uniquecode_type' ,$uniquecode_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Unique Code type delete form submit handler.
 */
function uniquecode_type_form_delete_confirm_submit($form, &$form_state) {
  $uniquecode_type = $form_state['uniquecode_type'];
  uniquecode_type_delete($uniquecode_type);

  watchdog('uniquecode_type', '@type: deleted %title.', array('@type' => $uniquecode_type->type, '%title' => $uniquecode_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $uniquecode_type->type, '%title' => $uniquecode_type->label)));

  $form_state['redirect'] = 'admin/structure/uniquecode-types';
}

/**
 * Page to select uniquecode Type to add new uniquecode.
 */
function uniquecode_admin_add_page() {
  $items = array();
  foreach (uniquecode_types() as $uniquecode_type_key => $uniquecode_type) {
    $items[] = l(entity_label('uniquecode_type', $uniquecode_type), 'admin/structure/uniquecode/add/' . $uniquecode_type_key);
  }
  return array('list' => array('#theme' => 'item_list', '#items' => $items, '#title' => t('Select type of uniquecode to create.')));
}

/**
 * Add new uniquecode page callback.
 */
function uniquecode_add($type) {
  $uniquecode_type = uniquecode_types($type);

  $uniquecode = entity_create('uniquecode', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('uniquecode_type', $uniquecode_type))));

  $output = drupal_get_form('uniquecode_form', $uniquecode);

  return $output;
}

/**
 * Unique Code Form.
 */
function uniquecode_form($form, &$form_state, $uniquecode) {
  $form_state['uniquecode'] = $uniquecode;

  $form['code'] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('Code'),
    '#default_value' => $uniquecode->code,
    '#states' => array(
      'visible' => array(
       ':input[name="batch_generation"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $uniquecode->description,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#default_value' => $uniquecode->uid,
  );

  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#default_value' => $uniquecode->status,
  );

  field_attach_form('uniquecode', $uniquecode, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#submit' => $submit + array('uniquecode_form_submit'),
  );

  // Show Delete button if we edit uniquecode.
  $uniquecode_id = entity_id('uniquecode', $uniquecode);
  if (!empty($uniquecode_id) && uniquecode_access('edit', $uniquecode)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('uniquecode_form_submit_delete'),
    );
  }

  // Only on node add
  if (empty($uniquecode->id)) {

    $form['actions']['submit']['#states'] = array(
      // Hide the settings when the cancel notify checkbox is disabled.
      'visible' => array(
       ':input[name="batch_generation"]' => array('checked' => FALSE),
      ),
    );

    $form['batch_generation'] = array(
      '#type' => 'checkbox',
      '#title' => t('Multiple generation'),
      '#default_value' => 0,
    );

    $form['batch_options'] = array(
      '#type' => 'fieldset',
      '#title' => t('Bulk options'),
      '#weight' => 99,
      '#tree' => TRUE,
      '#states' => array(
        // Hide the settings when the cancel notify checkbox is disabled.
        'visible' => array(
         ':input[name="batch_generation"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['batch_options']['mode'] = array(
      '#title' => t('Mode'),
      '#type' => 'radios',
      '#options' => array(
        'user_password' => t('Drupal user password function'),
        //'pattern' => t('Pattern'),
      ),
      '#required' => TRUE,
      '#default_value' => 'user_password',
    );

    $form['batch_options']['number'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of codes'),
    );

    $form['batch_options']['prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Prefix'),
    );

    $form['batch_options']['length'] = array(
      '#type' => 'textfield',
      '#title' => t('Code length'),
      '#default_value' => 10,
    );

    $form['actions']['batch'] = array(
      '#type' => 'submit',
      '#value' => t('Batch generation'),
      '#submit' => array('uniquecode_form_batch_submit'),
      '#states' => array(
        // Hide the settings when the cancel notify checkbox is disabled.
        'visible' => array(
         ':input[name="batch_generation"]' => array('checked' => TRUE),
        ),
      ),
    );
  }

  $form['#validate'][] = 'uniquecode_form_validate';

  return $form;
}

/**
 * Validate: uniquecode_form().
 */
function uniquecode_form_validate(&$form, &$form_state) {

  if (!empty($form_state['values']['batch_generation'])) {
    // Check batch code length:
    $length = $form_state['values']['batch_options']['length'];
    $prefix = $form_state['values']['batch_options']['prefix'];

    // @TODO: smart function to calculate how many characters we need to generate all codes.

    if (strlen($prefix) >= $length) {
      $text = t('Code lenght must be higher.');
      form_set_error('batch_options][prefix', $text);
      form_set_error('batch_options][length', $text);
    }
  }

}

/**
 * Unique Code submit handler.
 */
function uniquecode_form_submit($form, &$form_state) {
  $uniquecode = $form_state['uniquecode'];

  entity_form_submit_build_entity('uniquecode', $uniquecode, $form, $form_state);

  // Autogenerate empty CODES:
  if (empty($uniquecode->code)) {
    $uniquecode->code = user_password();
  }

  uniquecode_save($uniquecode);

  $uniquecode_uri = entity_uri('uniquecode', $uniquecode);

  $form_state['redirect'] = $uniquecode_uri['path'];

  drupal_set_message(t('Unique Code %title saved.', array('%title' => entity_label('uniquecode', $uniquecode))));
}

/**
 * Unique Code submit handler.
 */
function uniquecode_form_batch_submit($form, &$form_state) {
  // Prepare uniquecode entity:
  $uniquecode = $form_state['uniquecode'];
  entity_form_submit_build_entity('uniquecode', $uniquecode, $form, $form_state);

  $data = array(
    'uniquecode' => $uniquecode,
  );

  $batch = array(
    'title' => t('uniquecode batch generation'),
    'operations' => array(
      array(
        'uniquecode_generate_batch', array(
          $data,
          $form_state['values']['batch_options']
        )
      ),
    ),
    'progress_message' => t('Import. Operation @current out of @total.'),
    'error_message' => t('Error!'),
    'finished' => 'uniquecode_generate_batch_finished',
  );

  batch_set($batch);
}

/**
 * Submit: uniquecode_form_submit_delete
 */
function uniquecode_form_submit_delete($form, &$form_state) {
  $uniquecode = $form_state['uniquecode'];
  $uniquecode_uri = entity_uri('uniquecode', $uniquecode);
  $form_state['redirect'] = $uniquecode_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function uniquecode_delete_form($form, &$form_state, $uniquecode) {
  $form_state['uniquecode'] = $uniquecode;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['uniquecode_type_id'] = array('#type' => 'value', '#value' => entity_id('uniquecode' ,$uniquecode));
  $uniquecode_uri = entity_uri('uniquecode', $uniquecode);
  return confirm_form($form,
    t('Are you sure you want to delete uniquecode %title?', array('%title' => entity_label('uniquecode', $uniquecode))),
    $uniquecode_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function uniquecode_delete_form_submit($form, &$form_state) {
  $uniquecode = $form_state['uniquecode'];
  uniquecode_delete($uniquecode);

  drupal_set_message(t('Unique Code %title deleted.', array(
    '%title' => entity_label('uniquecode', $uniquecode))));

  $form_state['redirect'] = '<front>';
}
