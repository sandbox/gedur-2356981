UNIQUECODE
----------

This module provides an entity for multiple code generation.

HOW TO START:

1. Define your own code types in:
  /admin/structure/uniquecode-types/add

2. (Optional) Add your custom fields to the entity.

3. Create your code:
  You can create a Single code or multiples codes in a batch process.
  /admin/structure/uniquecode/add/{your-code-type} 

