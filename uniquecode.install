<?php

/**
 * Implements hook_schema().
 */
function uniquecode_schema() {
  $schema = array();

  $schema['uniquecode'] = array(
    'description' => 'The base table for uniquecodes.',
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The type (bundle).',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'code' => array(
        'description' => 'The code of the uniquecode.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'ID of Drupal user creator.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'Boolean indicating whether the code is active.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 1,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the uniquecode was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the uniquecode was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'description' => array(
        'description' => 'uniquecode description.',
        'type' => 'text',
      ),
    ),
    'primary key' => array('id'),
    'unique keys' => array(
      'code' => array('code'),
    ),
  );

  $schema['uniquecode_type'] = array(
    'description' => 'Stores information about all defined uniquecode types.',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique uniquecode type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );

  return $schema;
}
