<?php

/**
 * Task view callback.
 */
function uniquecode_view($uniquecode) {
  drupal_set_title(entity_label('uniquecode', $uniquecode));
  return entity_view('uniquecode', array(entity_id('uniquecode', $uniquecode) => $uniquecode), 'full');
}
